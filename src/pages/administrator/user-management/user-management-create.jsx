import React from "react";

export function UserManagementCreate() {
    return (
        <div className="mt-6 mx-6 overflow-hidden rounded-lg shadow-lg bg-white">
            <div className="bg-green-800 h-12 flex items-center p-6 font-medium text-white">
                Create User
            </div>
            <div className="m-5 border">
                // Content Here
            </div>
        </div>
    );
}

export default UserManagementCreate;
