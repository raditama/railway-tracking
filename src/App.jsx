import { Routes, Route, useLocation, Navigate } from "react-router-dom";
import { Navbar, Footer, Sidebar } from "./widgets/layout";
import { useState } from "react";
import { Home } from "./pages/dashboard";
import { Login } from "./pages/auth";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import UserManagement from "@/pages/administrator/user-management/user-management-index";
import { UserManagementCreate } from "./pages/administrator";

function App() {
    const [openMaps, setOpenMaps] = useState(false);
    const { pathname } = useLocation();
    const [page] = pathname.split("/").filter((el) => el !== "");

    const isShow = page != "login";

    return (
        <div className="bg-gray-200 min-h-screen flex flex-col items-between">
            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar
                newestOnTop={false}
                closeOnClick={false}
                rtl={false}
                pauseOnFocusLoss
                draggable={false}
                theme="dark"
            />
            {isShow && <Sidebar page={page} setOpenMaps={setOpenMaps} />}
            <div className={`${isShow && 'xl:ml-[240px]'} min-h-[calc(100vh-68px)]`}>
                {isShow && <Navbar openMaps={openMaps} setOpenMaps={setOpenMaps} />}
                <Routes>
                    <Route path="/login" element={<Login />} />
                    <Route path='/' element={<Navigate to="/login" />} />
                    <Route path="/dashboard" element={<Home />} />

                    <Route path="/user-management" element={openMaps ? <div /> : <UserManagement />} />
                    <Route path="/user-management/create" element={openMaps ? <div /> : <UserManagementCreate />} />

                    <Route path="*" element={openMaps ? <div /> : <PageNotFound />} />
                </Routes>
            </div>
            {isShow && <div className="xl:ml-[258px] text-blue-gray-600 mt-7 px-5">
                <Footer />
            </div>}
        </div>
    );
}

function PageNotFound() {
    return (
        <section className="flex items-center h-full p-16 text-gray-700">
            <div className="container flex flex-col items-center justify-center px-5 mx-auto my-8">
                <div className="max-w-md text-center">
                    <h2 className="mb-8 font-extrabold text-6xl dark:text-gray-600">
                        <span className="sr-only">Error</span>404
                    </h2>
                    <p className="text-xl font-semibold">Sorry, we couldn't find this page.</p>
                    <p className="mt-2 mb-8 text-sm">But dont worry, you can find plenty of other things on our homepage.</p>
                    <a rel="noopener noreferrer" href="/" className="px-8 py-3 font-semibold rounded">Back to homepage</a>
                </div>
            </div>
        </section>
    );
}

export default App;