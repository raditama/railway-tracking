const MENUS = [
    {
        id: 5,
        menu_name: "Administrator",
        icon: "GifIcon",
        route: "",
        slug: "administrator.index",
        permission: [],
        children: [
            {
                id: 1,
                c_menu_name: "User Management",
                c_icon: "UserIcon",
                c_route: "/user-management",
                c_slug: "user-management.index",
                c_parent_id: 5,
                created_at: null,
                updated_at: null
            },
            {
                id: 2,
                c_menu_name: "Role",
                c_icon: "GifIcon",
                c_route: "/role",
                c_slug: "role.index",
                c_parent_id: 5,
                created_at: null,
                updated_at: null
            },
            {
                id: 3,
                c_menu_name: "Registrasi Perangkat Perangkat",
                c_icon: "GifIcon",
                c_route: "/registrasi-perangkat",
                c_slug: "registrasi-perangkat.index",
                c_parent_id: 5,
                created_at: null,
                updated_at: null
            },
            {
                id: 4,
                c_menu_name: "Master Locotrack",
                c_icon: "GifIcon",
                c_route: "/locotrack",
                c_slug: "locotrack.index",
                c_parent_id: 5,
                created_at: null,
                updated_at: null
            }
        ]
    },
    {
        id: 6,
        menu_name: "Monitoring",
        icon: "GifIcon",
        route: "",
        slug: "monitoring.index",
        permission: [],
        children: [
            {
                id: 7,
                c_menu_name: "DAOP 1",
                c_icon: "GifIcon",
                c_route: "/monitor3",
                c_slug: "daop1.index",
                c_parent_id: 6,
                created_at: null,
                updated_at: null
            },
            {
                id: 8,
                c_menu_name: "DAOP 2",
                c_icon: "",
                c_route: "/monitor2",
                c_slug: "daop2.index",
                c_parent_id: 6,
                created_at: null,
                updated_at: null
            },
            {
                id: 9,
                c_menu_name: "DAOP 3",
                c_icon: "GifIcon",
                c_route: "/monitor",
                c_slug: "daop3.index",
                c_parent_id: 6,
                created_at: null,
                updated_at: null
            }
        ]
    },
    {
        id: 10,
        menu_name: "Kondisi Perangkat",
        icon: "EyeIcon",
        route: "/ini-single-menu",
        slug: "kondisi_perangkat.index",
        permission: [],
        children: []
    }
];

export default MENUS;
