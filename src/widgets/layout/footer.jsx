import PropTypes from "prop-types";
import { Typography } from "@material-tailwind/react";
import { HeartIcon } from "@heroicons/react/24/solid";

export function Footer({ brandName, brandLink, routes }) {
    return (
        <div className="text-xs text-right py-3">
            © 2023 System Information, PT.Kereta Api Indonesia (Persero)
        </div>
    );
}

export default Footer;
