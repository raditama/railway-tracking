export * from "@/widgets/layout/sidebar";
export * from "@/widgets/layout/navbar";
export * from "@/widgets/layout/configurator";
export * from "@/widgets/layout/footer";