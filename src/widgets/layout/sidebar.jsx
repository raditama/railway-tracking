import { Link, NavLink } from "react-router-dom";
import { ChevronDownIcon, XMarkIcon } from "@heroicons/react/24/outline";
import {
    Avatar,
    IconButton,
    Typography,
} from "@material-tailwind/react";
import { useMaterialTailwindController, setOpenSidenav } from "@/context";
import "./styles/sidebar.css";
import MENUS from "@/routes";
import { ChevronUpIcon, GifIcon } from "@heroicons/react/24/solid";
import { useEffect, useState } from "react";
import * as HIS from "@heroicons/react/24/solid";
import * as HIO from "@heroicons/react/24/outline";

export function Sidebar({ page, setOpenMaps }) {
    const [controller, dispatch] = useMaterialTailwindController();
    const { sidenavType, openSidenav } = controller;
    const sidenavTypes = {
        dark: "bg-blue-800",
        white: "bg-white shadow-lg",
        transparent: "bg-transparent",
    };

    const [menuShow, setMenuShow] = useState("");

    function _clickParentMenu(value) {
        if (value == menuShow) setMenuShow("");
        else setMenuShow(value);
    }

    function _clickChildrenMenu(value) {
        setOpenMaps(false);

    }

    useEffect(() => {
        MENUS.map(item => {
            item.children.map(item2 => {
                if (item2.c_route == "/" + page) {
                    setMenuShow(item.menu_name);
                }
            })
        })
    }, [])

    return (
        <aside
            className={`${sidenavTypes[sidenavType]} ${openSidenav ? "translate-x-0" : "-translate-x-80"
                } fixed inset-0 z-50 h-100 w-60 transition-transform duration-300 xl:translate-x-0`}
        >
            <div
                className={`relative border-b ${sidenavType === "dark" ? "border-white/20" : "border-blue-gray-50"
                    }`}
            >
                <Link className="flex items-center gap-4 py-6 px-8">
                    <Avatar src="/img/logo-ct.png" size="sm" />
                    <Typography
                        variant="h6"
                        color={sidenavType === "dark" ? "white" : "blue-gray"}
                    >
                        Material Tailwind React
                    </Typography>
                </Link>
                <IconButton
                    variant="text"
                    color="white"
                    size="sm"
                    ripple={false}
                    className="absolute right-0 top-0 grid rounded-br-none rounded-tl-none xl:hidden"
                    onClick={() => setOpenSidenav(dispatch, false)}
                >
                    <XMarkIcon strokeWidth={2.5} className="h-5 w-5 text-white" />
                </IconButton>
            </div>

            <div className="overflow-scroll max-h-[calc(100vh-120px)] scroll-custom flex flex-col gap-1 text-white text-xs">
                {MENUS.map((item, key) => {
                    const Icon = item.icon ? HIS[item.icon] : HIO['StopIcon'];
                    return (<>
                        {item.children.length > 0 ?
                            <>
                                <button className="bg-blue-500 w-full flex items-center gap-4 px-4 py-3 capitalize rounded-none bg-opacity-20 grid grid-cols-6"
                                    onClick={() => _clickParentMenu(item.menu_name)}>
                                    <div className="w-42"><GifIcon width={20} /></div>
                                    <div className="capitalize text-left col-span-4">{item.menu_name}</div>
                                    <div className="w-42 flex items-center justify-end">
                                        {menuShow != item.menu_name ? <ChevronDownIcon width={16} /> : <ChevronUpIcon width={16} />}
                                    </div>
                                </button>
                                <ul className={`space-y-2 ${menuShow != item.menu_name && 'hidden'}`}>
                                    {item.children.map((item2, key2) => {
                                        const Icon2 = item2.c_icon ? HIS[item2.c_icon] : HIO['StopIcon'];
                                        return (
                                            <NavLink to={`${item2.c_route}`}>
                                                {({ isActive }) => (
                                                    <button className={`bg-blue-500 w-full flex items-center gap-4 px-4 py-3 capitalize rounded-none hover:bg-blue-500 ${!isActive && 'bg-opacity-0'}`} onClick={() => setOpenMaps(false)}>
                                                        <div className="w-42">&#8226;</div>
                                                        <div className="w-42"><Icon2 width={20} /></div>
                                                        <div className="capitalize text-left">{item2.c_menu_name}</div>
                                                    </button>
                                                )}
                                            </NavLink>
                                        )
                                    })}
                                </ul>
                            </>
                            :
                            <NavLink to={`${item.route}`}>
                                {({ isActive }) => (
                                    <button className={`bg-blue-500 w-full flex items-center gap-4 px-4 py-2 capitalize rounded-none hover:bg-blue-500 ${!isActive && 'bg-opacity-0'}`} onClick={() => setOpenMaps(false)}>
                                        <div className="w-42"><Icon width={20} /></div>
                                        <div className="capitalize text-left">{item.menu_name}</div>
                                    </button>
                                )}
                            </NavLink>
                        }
                    </>)
                })}
            </div>
        </aside>
    );
}

Sidebar.displayName = "/src/widgets/layout/sidnave.jsx";

export default Sidebar;
