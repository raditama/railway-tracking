import { Link, useLocation } from "react-router-dom";
import {
    IconButton,
    Menu,
    MenuHandler,
    MenuList,
    MenuItem,
} from "@material-tailwind/react";
import {
    UserIcon,
    Bars3Icon,
    PencilIcon,
    ArrowRightOnRectangleIcon,
} from "@heroicons/react/24/solid";
import {
    setOpenSidenav,
    useMaterialTailwindController,
} from "@/context";
import LOGO_KAI from '../../../public/img/logo/logo_kai.png';
import ClockLive from "../component/clock-live";
import MonitoringGps from "@/layouts/monitoring-gps";

export function Navbar({ openMaps, setOpenMaps }) {
    const [controller, dispatch] = useMaterialTailwindController();
    const { openSidenav } = controller;
    const { pathname } = useLocation();
    const [page] = pathname.split("/").filter((el) => el !== "");

    return (
        <div className={`sticky bg-white bg-opacity-60 top-0 z-40 shadow-none flex flex-col gap-2 ${openMaps ? '' : ''}`}>
            <div className="flex justify-between gap-6 md:flex-row md:items-center bg-white shadow-md shadow-blue-gray py-3 px-5">
                <div className="flex items-center w-[40%] gap-3">
                    <IconButton
                        variant="text"
                        color="blue-gray"
                        className="grid xl:hidden"
                        onClick={() => setOpenSidenav(dispatch, !openSidenav)}
                    >
                        <Bars3Icon strokeWidth={3} className="h-6 w-6 text-blue-gray-500" />
                    </IconButton>
                    <div>
                        <img src={LOGO_KAI} className="w-20" />
                    </div>
                    <div className="flex flex-col">
                        <div className="font-bold text-md">Railways Tracking</div>
                        <div className="font-medium text-xs">PT. KERETA API INDONESIA (PERSERO)</div>
                    </div>
                </div>
                <div className="flex w-[20%] justify-center items-center gap-2 font-semibold text-md">
                    <ClockLive />
                </div>
                <div className="flex items-center w-[40%] justify-end align-center gap-2">
                    <div>
                        <div className="text-end font-medium text-xs">JOHN (911)</div>
                        <div className="text-end font-medium text-xs">DEVELOPER BD (Bandung) - DAOP 2</div>
                    </div>
                    <Menu>
                        <MenuHandler>
                            <IconButton variant="text" color="blue-gray">
                                <UserIcon className="h-6 w-6 text-blue-gray-500" />
                            </IconButton>
                        </MenuHandler>
                        <MenuList className="w-max border-0 text-xs p-2">
                            <MenuItem>
                                <Link to="/change-password" className="flex items-center gap-2">
                                    <PencilIcon className="h-4 w-4" />
                                    <div>Change Password</div>
                                </Link>
                            </MenuItem>
                            <MenuItem className="flex items-center gap-2">
                                <ArrowRightOnRectangleIcon className="h-4 w-4" />
                                <div>Logout</div>
                            </MenuItem>
                        </MenuList>
                    </Menu>
                </div>
            </div>
            <div className="px-5">
                <div className="flex">
                    <div className="p-1 bg-gray-200 rounded-lg flex">
                        <div onClick={() => setOpenMaps(true)} className={`capitalize text-xs py-1 px-4 rounded-l-md cursor-pointer ${openMaps ? 'bg-white' : "bg-gray-200"}`}>
                            Monitoring GPS
                        </div>
                        <div onClick={() => setOpenMaps(false)} className={`capitalize text-xs py-1 px-4 rounded-r-md cursor-pointer ${!openMaps ? 'bg-white' : "bg-gray-200"}`}>
                            {page?.replace('-', ' ')}
                        </div>
                    </div>
                </div>
            </div>
            <div className={!openMaps && 'h-0 overflow-hidden'}>
                <MonitoringGps />
            </div>
        </div>
    );
}

Navbar.displayName = "/src/widgets/layout/dashboard-navbar.jsx";

export default Navbar;