import { CalendarDaysIcon, ClockIcon } from "@heroicons/react/24/outline";
import moment from "moment/moment";
import { useEffect, useState } from "react";

function ClockLive() {
    const [times, setTimes] = useState(moment().format('HH:mm:ss'));
    const [dates, setDates] = useState(moment().format('DD MMMM YYYY'));

    useEffect(() => {
        const interval = setInterval(() => {
            var time = moment().format('HH:mm:ss');
            var date = moment().format('DD MMMM YYYY');

            setTimes(time);
            setDates(date);
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    return (
        <div className="flex justify-center items-center flex-col">
            <div className="flex gap-2 justify-center items-center"><ClockIcon className="h-5" />{times}</div>
            <div className="flex gap-2 justify-center items-center"><CalendarDaysIcon className="h-5" />{dates}</div>
        </div>
    )
}

export default ClockLive; 